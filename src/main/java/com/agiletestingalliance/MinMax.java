package com.agiletestingalliance;

public class MinMax {

	public int f(int a, int b) {
		if (b > a)
			return b;
		else
			return a; 
	}

	
	public String bar(String string) {
		if (string != null && !string.equals("")) {
            return string;
        } else {
            return ""; // Returning an empty string when the condition is not met.
        }
	}
	

}
